# Changelog
[What for?](https://keepachangelog.com/en/1.1.0/)

## Change types

- `Added` for new features.
- `Changed` for changes in existing functionality.
- `Deprecated` for soon-to-be removed features.
- `Removed` for now removed features.
- `Fixed` for any bug fixes.
- `Security` in case of vulnerabilities.


---

## [1.4.1] 2020-04-25

### Changed

- Fading polling. Earlier if http polling was active (Aphlict unavailable) requests were performed every 15 seconds. Now the polling interval grows until a __new__ notification is received. When new alert comes, interval will be reset to its minimum — 25 seconds. And after each polling iteration which brings no new notifications interval will be doubled but no grater than maximum — 15 minutes.
It worth to note that if new notification comes at some time but remains unread, it won't be cause of polling interval reset on next polling iteration.

## [1.3.3] 2020-04-25

### Fixed

- Only one active instance (among of all opened tabs) performs Aphlict availability checks.

## [1.3.2] 2020-04-25

### Added

- Ability to build Notifier for different Phabricator instances [#1](https://bitbucket.org/vbelozyorov/phab-notifier/issues/1)

### Changed

- Notifier determines protocol (`ws` or `wss`) for Aphlict availability check based on `location.protocol` value (`http` or `https`)

## [1.2.0] 2020-04-25

### Changed

- Notifier checks if Phabricator's native Aphlict server is available. If so, Notifier assumes that notifications are going to be delivered through websocket and won't poll notifiactions endpoint. Notifier periodically (5 minutes) repeats this Aphlict availability check and fallback to http polling if it turns out that Aphlict isn't available anymore. And stops polling again if Aphlict comes available, and so on…
- If browser doesn't support desktop notifications Notifier only deactivates its notification delivery features instead of fully stops working as before. For example, it keeps adding `mark as read` buttons to notifications.

## [1.1.0] 2019-08-20

### Fixed

- marking notifications as read works from notifications menu too

## [1.0.0] 2019-08-08

### Added

- Buttons for marking notifications as read
