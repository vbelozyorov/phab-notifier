// ==UserScript==
// @name         phab-notifier
// @namespace    https://bitbucket.org/vbelozyorov/phab-notifier
// @version      1.4.2.%BUILD_TIME%
// @description  Phabricator notifications improvements
// @author       Vladimir Belozyorov
// @match        http://%PHAB_HOST%/*
// @match        https://%PHAB_HOST%/*
// @grant        none
// @downloadURL  https://bitbucket.org/vbelozyorov/phab-notifier/downloads/%PHAB_HOST%_notifier.user.js
// @updateURL    https://bitbucket.org/vbelozyorov/phab-notifier/downloads/%PHAB_HOST%_notifier.user.js
// @supportURL   https://bitbucket.org/vbelozyorov/phab-notifier/issues
// ==/UserScript==

(function() {
    'use strict';

    let SELF_NAME = GM_info.script.name;
    let PHAB_HOST = '%PHAB_HOST%';

    let heartbeatDelay = 1000 * 5;
    let mainDelay = {
        min: 1000 * 25,             // 25 seconds
        max: 1000 * 60 * 15,        // 15 minutes
        actual: null
    };
    mainDelay.actual = mainDelay.max;

    let aphlictTestDelay = 1000 * 60 * 5;
    let taskRenotifyDelay = 1000 * 60 * 30;

    let timestart = Date.now();
    let pause = 0;
    let previousPause = null;
    let isRunning = true;
    let activeNotifications = {};
    let isAphlictAvailable = null;

    let template = document.createElement('template');

    let similarBrowsers = getSimilarBrowsers();

    function heartbeat() {
        let running = {};
        let now = Date.now();
        pause = 0;
        if ('notifier-running' in localStorage ) {
            running = JSON.parse(localStorage['notifier-running']);
            //console.debug(running);
            for (let ts in running) {
                let hb = running[ts];
                if (hb < now-2*heartbeatDelay) {
                    delete running[ts];
                } else {
                    if (ts >= timestart) {
                        pause += 0;
                    } else {
                        pause += 1;
                        break;
                    }
                }
            }
        }
        running[timestart] = now;
        localStorage['notifier-running'] = JSON.stringify(running);
    }

    /**
     * @see heartbeat()
     * @returns {Boolean}
     */
    function isInstanceRunning() {
        if (pause !== previousPause) {
            if (pause > 0) {
                console.info(SELF_NAME + " in this tab goes to sleep while other tab(s) running");
                isRunning = false;
            } else {
                console.info(SELF_NAME + " in this tab starts working now");
                isRunning = true;
            }
            previousPause = pause;
        }

        return isRunning;
    }

    function testAphlictAvailability() {
        if (!isInstanceRunning()) {
            return;
        }

        let prevState = isAphlictAvailable;
        let protocol = location.protocol.replace('http', 'ws');

        let testAphlict = new Promise((resolve, reject) => {
            let socket = new WebSocket(protocol + "//" + location.host + "/ws/");
            socket.onopen = function() {
                socket.close();
                resolve();
            };

            socket.onerror = function(error) {
                reject();
            };
        });

        testAphlict
            .then(() => {
                isAphlictAvailable = true;
            })
            .catch(() => {
                isAphlictAvailable = false;
            })
            .finally(() => {
                if (prevState !== isAphlictAvailable) {
                    console.info('Aphlict server is ' + (isAphlictAvailable ? 'available' : 'not available'));
                }
            })
    }


    /**
     * https://stackoverflow.com/a/35385518/1764747
     *
     * @param {String} HTML representing a single element
     * @return {Element}
     */
    function htmlToElement(html) {
        template = document.createElement('template');
        html = html.trim(); // Never return a text node of whitespace as the result
        template.innerHTML = html;
        return template.content.firstChild;
    }

    /**
     * @param {String} HTML representing any number of sibling elements
     * @return {NodeList}
     */
    function htmlToElements(html) {
        template = document.createElement('template');
        template.innerHTML = html;
        return template.content.childNodes;
    }

    function showNotification(element) {
        var anchors = element.querySelectorAll('a.phui-handle:not(.phui-link-person)');
        var taskHref = element.innerText.includes('subtask of') ? anchors[anchors.length-1] : anchors[0];
        var task = taskHref.getAttribute('href').replace('/', '');

        var options = {
            icon: 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAGAAAABgCAMAAADVRocKAAACr1BMVEUAAAANGxsUFBQUGR0WGx8WGyAWHCAXHCAWGx4WGx8XHCAVGx8WGx8XHCAXHB8XHCAWGx8XGx8XHCAXGx8XHCAXHCAXGyAXHCAXHCAXHCAXHCAXHCAXHCAXHCAXHCAXHCAXGyAXHCAXHCAXHCAXHCAXHCAXHCAXHCAXHCAXHCAXHCAXHCAXHCAXHCAXHCAXHCAXHCAXHCAXHCAXHCAXHCAXHCAXHCAXHCAXHCAXHCAXHCAXHCAXHCAXHCAXHCAXHCAXHCAXHCAXHCAXHCAXHCAXHCAXHCAXHCAXHCAXHCAXHCAXHCAXHCAXHCAXHCAXHCAXHCAXHCAXHCAXHCAXHCAXHCAXHCAXHCAXHCAXHCAXHCAXHCAXHCAXHCAXHCAXHCAXHCAXHCAXHCAXHCAXHCAXHCAXHCAXHCAXHCAXHCAXHCAXHCAXHCAXHCAXHCAXHCAXHCAXHCAXHCAXHCAXHCAXHCAXHCAXHCAXHCAXHCAXHCAXHCAXHCAXHCAXHCAXHCAXHCAXHCAXHCAXHCAXHCAXHCAXHCAXHCAXHCAXHCAXHCAXHCAXHCAXHCAXHCAXHCAXHCAXHCAXHCAXHCAXHCAXHCAXHCAXHCAXHCAXHCAXHCAXHCAXHCAXHCAXHCAXHCAXHCAXHCAXHCAXHCAXHCAXHCAXHCAXHCAXHCAXHCAXHCAXHCAXHCAXHCAXHCAXHCAXHCAXHCAXHCAXHCAXHCAXHCAXHCAXHCAXHCAXHCAXHCAXHCAXHCAXHCAXHCAXHCAXHCAXHCAXHCAXHCAXHCAXHCAXHCAXHCAXHCAXHCAXHCAXHCAXHCAXHCAXHCAXHCAXHCAXHCAXHCAXHCAXHCAXHCAXHCAXHCAXHCAXHCAXHCAXHCAXHCAXHCAXHCAXHCAXHCAXHCAXHCAXHCC48bs8WqCiAAAA43RSTlMAAAAAAQEBAQICAgMDAwQFBwcHCAgJCgoLDA4PEBESExQUFRYXGBkaGxwdHh8gISIjJCUmJygpKywtLi8wMjQ1Njc4OTo7PD0+P0FCREZHSElKS0xNT1BRUlZXWFlbXF1eX2BhYmNkZmlqa21ub3JzdHV2d3l8fX5/gIGCg4SGiYqLjI2Oj5CRkpOUlZaXmJmanJ2eoqSlp6ipqqytrrG0tba3uLm6u7y9vr/AwcLDxMXIycrLzM3Oz9DR0tPU1dbX2Nna29zd3t/g4uPk5ebn6Onq7O3u7/Dx8vP09fb3+/z9/mMghu4AAAR0SURBVHgB7dnpX1RVGAfwX4vZTotGas3EDAsaKWpWmLaYS6WVUanZki1KklhqWVJZoaQVOFqQLZmiFmSkBbIgaiZKjlYCqWAwBfP7R/oM3HPnnjvD5dxh8NV83w6fszz3nOc854CYmJiYmHPqirGPZa/wbNzoWbE4M/1yRNfgqavKfDToKH1ryoWIlolrmxlG05oMRMEFj5SzV+UPn4d+mlZDS9X3oT/cn7NPxW5EbO4ZKjg1B5GJy6fkxPYNOVnPPZu1pLDkOCVr4xCB4T8wyL8jewwMxrxc0sWgsuGwLaGGuuZ33QjhfvtP6moSYJPrAIW2ZdchrKGvnqbQeLPN9vdR2JxoMc1iCr+nwoakOn3482Fpbis1R0dBmVMfv3c8+jC2kZqGFPvfd2+iwl9XUPNbEpQ49ParhkHBsEpqDrqV2tfj702GEtcxag64FGZcS40vA5LpTmgmDYHk1rP6HFzq8eFLMLghp56Naei20N+SJy/7BRTqnKrx4XYY7exe62nd7XeR7JSn9y2FfU7F/NCRBqMyBnhHAU/4Q+eHke0UahOU4sM8hOmALVVV/7LbIkhWU1fjUIgP2xyQfEWZuYMRrdTVOyziI3wEmaePDpBP69wq4iNkQJLYQJNNkE0graIk4iPsh+CekjnrrmmHGGJ9XMD5EPbSoM4Bk4RqGuVC+JKWnoewkrSIUvwvlMyAJqWLlqog3GP64XoYfUhJ19XQvGn6oeY4ZVOhucRHSQEM0v+jpBpCMQ28868FRn/sZ9jlVkVJ5x0I+oCyzRDif6SuQsve8zqpO5IM4VP2PgXzvD3QDf2emvbU0LgdS4NuHWVN0N1Ik+UIGiI28ToIIzrE+EchKIcmSRAyaJIjZXv2mIeQcE+GQTZNJtvsYCZ0P7HH7YodWIYIi0IPILGxZ0EtRFYfGQvFqtwJ4U5qzkyX9pKsSWGZaueXZi56XPozhbMPQreJskKljZbpp66jp4dhXzPI94DFRrNIFVdB8xqNvntxxuzcFhqV6qmig5ICtWTnaKe1p9WSHZy9pWsPLbVcA80bpnSteuDc5ikq8nhOMdTBwyebG5ZBqJQPHJtH5sS/abYBkvHykWn70F9Mk22QrZHiY79sWUSTb9TLFovCy6KDMkjeoa7WEUnp+II2uCcXaBt2F4xSDaWjS7H43QGjKQxoSAawngF7YLSFQr0zsvLdvcVPertnNSjQg281DJ6hUH9TxBeQ0e/vFpeCVfuXSnEe10bNIffAXqF+dQ/IJbBCbz9xIK6xzuA1NhmKHDYu4ulHqDmcAmWJtapPCXNaKZcwilx1So8hjiIKR1NhS0K94TknHmENWXo62P5I2OSo7ONByrXqD+rqUmBb/FaLJ7VbsrZ1MqjciQhc9h4lJ0oKl2Q9NT/rlYKtXko+iUNkHj1JBf/MQ8Rcn7FPXyShP+7dQ0u1M9FfD5WzV7szByEKJuT9xTBa8ichWi66e2VpOw18u3LvH4zoujh99uLlgX+xvJ79+LgrERMTExNzLv0PzvVjW4j/270AAAAASUVORK5CYII=',
            body: element.innerText + ((similarBrowsers.indexOf('firefox') !== -1) ? ' <a href="' + taskHref.href + '">look</a>' : ''),
            tag: task
            //renotify: true
        }
        //console.debug(options);
        var notification = new Notification(taskHref.innerText, options);
        notification.onclick = function(event) {
            event.preventDefault();
//            console.debug('notification onclick', event, taskHref.href, this);
            window.open(taskHref.href);
        }
//        notification.onclose = function(event) {console.debug('onclose', notification, event);};
//        notification.onerror = function(event) {console.debug('onerror', notification, event);};
//        notification.onshow = function(event) {console.debug('onshow', notification, event);};
        activeNotifications[task] = notification;
//        console.debug(activeNotifications);
    }

    function sha1(str) {
        // We transform the string into an arraybuffer.
        var buffer = new TextEncoder("utf-8").encode(str);
        return crypto.subtle.digest("SHA-1", buffer).then(function (hash) {
            return hex(hash);
        });
    }

    function hex(buffer) {
        var hexCodes = [];
        var view = new DataView(buffer);
        for (var i = 0; i < view.byteLength; i += 4) {
            // Using getUint32 reduces the number of iterations needed (we process 4 bytes each time)
            var value = view.getUint32(i)
            // toString(16) will give the hex representation of the number without padding
            var stringValue = value.toString(16)
            // We use concatenation and slice for padding
            var padding = '00000000'
            var paddedValue = (padding + stringValue).slice(-padding.length)
            hexCodes.push(paddedValue);
        }

        // Join all the hex strings into one
        return hexCodes.join("");
    }

    /**
     * https://stackoverflow.com/a/9851769/1764747
     *
     * @returns []
     */
    function getSimilarBrowsers() {
        var browsers = [];

        // Opera 8.0+
        if ((!!window.opr && !!opr.addons) || !!window.opera || navigator.userAgent.indexOf(' OPR/') >= 0) {
            browsers.push('opera');
        }

        // Firefox 1.0+
        if (typeof InstallTrigger !== 'undefined') {
            browsers.push('firefox')
        }

        // Safari 3.0+ "[object HTMLElementConstructor]"
        if (/constructor/i.test(window.HTMLElement) || (function (p) { return p.toString() === "[object SafariRemoteNotification]"; })(!window['safari'] || safari.pushNotification)) {
            browsers.push('safari');
        }

         // Internet Explorer 6-11
        let isIE;
        if (isIE =/*@cc_on!@*/false || !!document.documentMode) {
            browsers.push('ie');
        }

        // Edge 20+
        if (!isIE && !!window.StyleMedia) {
            browsers.push('edge');
        }

        // Chrome 1+
        let isChrome;
        if (isChrome = !!window.chrome && !!window.chrome.webstore) {
            browsers.push('chrome');
        }

        // Blink engine detection
//        var isBlink = (isChrome || isOpera) && !!window.CSS;
        return browsers;
    }

    function markNotificationAsRead(clickEvent) {
        clickEvent.preventDefault();
        clickEvent.stopPropagation();

        let notification = clickEvent.target.closest('.phabricator-notification');
        let anchors = notification.querySelectorAll('a.phui-handle:not(.phui-link-person)');
        let anchor = notification.innerText.includes('subtask of') ? anchors[anchors.length-1] : anchors[0];
        let notificationHref = anchor.href;

        let reg = new RegExp('(\\s|^)' + 'phabricator-notification-unread' + '(\\s|$)');
        notification.className = notification.className.replace(reg, ' ');
        notification.querySelector('.mark-notificcation-as-read-btn').remove();

        let openedWindow = window.open(notificationHref, '_blank');
        setTimeout(() => {
            openedWindow.close();
        }, 200);
    }

    function addButtons() {
        let unreadFoots = document.querySelectorAll('.phabricator-notification-unread .phabricator-notification-foot');
        unreadFoots.forEach((footBlock) => {
            if (footBlock.querySelector('.mark-notificcation-as-read-btn')) {
                return;
            }
            let btn;
            if (footBlock.closest('div.phabricator-notification-menu')) {
                /**
                 * Seems because of Javelin JS library, handler for click on whole div.phabricator-notification executes on capture event phase
                 * before our custom handler for mark-as-read button. So user just being navigated to page which notification reffers to.
                 * That's why we are placing <a> instead of <button> for notifications inside div.phabricator-notification-menu.
                 */
                btn = htmlToElement('<a title="Mark as read" class="mark-notificcation-as-read-btn" style="display: inline-block; float: right; line-height: 15px;" href="#">✓</a>');
            } else {
                btn = htmlToElement('<button class="mark-notificcation-as-read-btn" title="Mark as read" style="display: inline; float: right; line-height: 10px;">✓</button>');
            }

            btn.addEventListener('click', markNotificationAsRead);
            footBlock.appendChild(btn);
        });
    }

    function main() {
        if (!isInstanceRunning() || isAphlictAvailable === true) {
            // no any hard work, so set small timeout for quick potential future wakeup
            setTimeout(main, mainDelay.min);
            return;
        }

        var xhr = new XMLHttpRequest();
        xhr.open('POST', location.protocol + '//' + location.host + '/notification/panel/');
        var xhrPromise = new Promise((resolve, reject) => {
            xhr.onreadystatechange = function() { // (3)
                if (xhr.readyState != 4) {
                    return;
                }

                if (xhr.status != 200) {
                    console.error('Notifications request failed (' + xhr.status + ': ' + xhr.statusText + ')');
                    reject();
                } else {
//                    console.debug('Notification request success', xhr.responseText);
                    resolve(xhr.responseText);
                }

            }
        });
        xhr.send();

        let wereNewAlertsReceived = false;

        xhrPromise
            .then((responseText) => {
                let braceIdx = responseText.indexOf("{");
                let json = responseText.substring(braceIdx);
                let responseObj = JSON.parse(json);
                template.remove();
                return htmlToElements(responseObj.payload.content);
            })
            .then((elements) => {
                let unread = [];
                let shown;
                if ('notifier-shown' in localStorage) {
                    shown = JSON.parse(localStorage['notifier-shown']);
                } else {
                    shown = {};
                }

//                console.debug(elements);
                let now = Date.now();
                let actualTasks = [];
                let elemPromises = [];
                elements.forEach((elem) => {
                    if (elem.matches('.phabricator-notification-unread')) {
                        let task = elem.childNodes[2].getAttribute('href').replace('/', '');
                        actualTasks.push(task);
                        elemPromises.push(
                            sha1(elem.textContent)
                                .then((digest) => {
                                    let show = true;
                                    if (task in shown) {
                                        if (digest in shown[task].notifications) {
                                            show = false;
                                        } else {
                                            // existent task but new event with it
                                            wereNewAlertsReceived = true;
                                        }
                                        if (now >= shown[task].renotify) {
                                            shown[task].renotify = now + taskRenotifyDelay;
                                            shown[task].notifications = {};
                                            show = true;
                                        }
                                    } else {
                                        // new task
                                        wereNewAlertsReceived = true;

                                        shown[task] = {};
                                        shown[task].renotify = now + taskRenotifyDelay;
                                        shown[task].notifications = {};
                                    }
                                    if (show) {
                                        shown[task].notifications[digest] = 1;
                                        unread.push(elem);
                                    }
                                })
                        );
                    }
                });

                return Promise.all(elemPromises)
                    .then(() => {
                        for (let task in shown) {
                            if (actualTasks.indexOf(task) === -1) {
                                delete shown[task];
                            }
                        }

                        localStorage['notifier-shown'] = JSON.stringify(shown);
                        return unread.reverse();
                    });
            })
            .then((unread) => {
//                console.debug(unread.length + " unread notifications");
                unread.forEach((el) => {
                    showNotification(el);
                });
            })
            .catch((reason) => {console.error(reason)})
        ;

        if (wereNewAlertsReceived) {
            // polling speed up
            mainDelay.actual = mainDelay.min;
        } else {
            // polling slow down
            mainDelay.actual = Math.min(mainDelay.max, mainDelay.actual * 2);
        }
        setTimeout(main, mainDelay.actual);
    };

    setInterval(addButtons, 2000);

    if (!("Notification" in window)) {
        console.warn("This browser does not support desktop notification. " + SELF_NAME + " has to disable notifiactions delivery features.");
    } else {
        let requestPromise = Promise.resolve(Notification.permission);
        if (Notification.permission == 'default') {
            requestPromise = Notification.requestPermission();
        }
        requestPromise
            .then((permission) => {
                if (permission === 'granted') {
                    // check Aphlict availability first to eliminate uncertainty
                    testAphlictAvailability();

                    /**
                     * After a heartbeat(), this instance may fall asleep and any
                     * requests to the server will not be executed until awakening.
                     * But after waking up, main() is likely to execute earlier
                     * than testAphlictAvailability(), and by this time we want
                     * to know the availability of Aphlict so as not to make
                     * unnecessary requests to the server
                     */
                    heartbeat();

                    let hbInterval = setInterval(heartbeat, heartbeatDelay);
                    let aphlictInterval = setInterval(testAphlictAvailability, aphlictTestDelay);
                    main();
                } else {
                    console.warn("Notifications permission is " + permission + ". " + SELF_NAME + " can't deliver notifications without your permission.");
                }
            })
            .catch((reason) => {
                console.error(reason);
            })
        ;
    }
})();
