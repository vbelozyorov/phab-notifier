# phab-notifier

Userscript that slightly improves Phabricator notification feature.

## Features

### «Mark as read» buttons

For notifications displayed in various places of Phabricator interface Notifier adds buttons by which you can mark particular notification as read.

### Notifications delivery

Notifier periodically checks if Phabricator's native notifications server (called Aphlict) is available for current Phabricator instance. If so, Notifier assumes that notifications are going to be delivered through `websocket` protocol and do nothing more than adds «Mark as read» buttons.

But if at some time it turns out that Aphlict isn't available Notifier start poll of Phabricator's notifications endpoint via http and shows unread notifications on your desktop.
And stops this polling if Aphlict comes available again, and so on.

## Requirements

- Modern browser
- [Tampermonkey](https://tampermonkey.net/) (or compatible) browser extension for userscripts support

## Installation

After you've installed choosen browser extension for userscripts support, simply [download Notifier](https://bitbucket.org/vbelozyorov/phab-notifier/downloads/Notifier.user.js) and accept script installation.

Also inside Phabricactor settings (https://<your Phabricator installation>/settings/user/<your Phabricator account>/page/emailpreferences/) make sure that `Notify` option is selected for desired events. In case of `Email` selected, notifications inside Phabricator will be created in state `Read` and Notifier won't notify you.

### Your Phabricator instance's hostname

By default it is `phab.shire.local`.

If it's not your case, you can [create proposal](https://bitbucket.org/vbelozyorov/phab-notifier/issues/new) where specify hostname of your Phabricator instance and we try to make special Notifier build for this instance.

Actually the only place depending on this hostname is `@match` directive in script's header. It tells to browser extension on which pages this particular script (`phab-notifier` in this case) should work. So when you open any page which URL doesn't matches to script's `@match` directive script just won't execute.

Thereby nothing stops you to modify script's `@match` directive by hand so it matches your Phabricator instance, but if you go this way you'll need to do it after every script update, so special build will just more convinient to use.

## Usage

When Notifier installed, open or refresh browser tab with Phabricator and accept request for notifications permission.

You need to keep open at least one browser tab with Phabricator. Notifier script will work in this tab: periodically (5 minutes) check Aphlict availability and (if Aphlict is not available) request Phabricactor backend to notify you on desktop about unread notifications (bell icon on top panel of Phabricator).
If there are several tabs with Phabricator, Notifier will work inside only one of them. When you close this tab, Notifier in another tab will start working. And so on until you close all Phabricator tabs.

If desktop notification about event in some task was displayed but event in Phabricator remains unread (i.e. you simply close notification) notification about this task will be shown again in 30 minutes. But in case another event with same task occurs earlier new desktop notification will be displayed immediately.
