// ==UserScript==
// @name         phab-tweaks
// @namespace    https://bitbucket.org/vbelozyorov/phab-notifier
// @version      1.0.0.%BUILD_TIME%
// @description  Some Phabricator usability tweaks
// @author       Vladimir Belozyorov
// @match        http://%PHAB_HOST%/T*
// @match        https://%PHAB_HOST%/T*
// @grant        none
// @downloadURL  https://bitbucket.org/vbelozyorov/phab-notifier/downloads/%PHAB_HOST%_tweaks.user.js
// @updateURL    https://bitbucket.org/vbelozyorov/phab-notifier/downloads/%PHAB_HOST%_tweaks.user.js
// @supportURL   https://bitbucket.org/vbelozyorov/phab-notifier/issues
// ==/UserScript==

(function() {
    'use strict';

    let currentTask = document.URL.split('/').reverse()[0]

    let anchor = document.createElement('a');
    anchor.href = '/maniphest/task/edit/form/1?template=' + currentTask

    let span = document.createElement('span');
    span.innerHTML = '&nbsp;'
    span.classList.add(...['visual-only', 'phui-icon-view', 'phui-font-fa', 'fa-plus'])

    anchor.appendChild(span)
    anchor.appendChild(document.createTextNode('Create similar task'));

    document.querySelector('div.phui-crumbs-actions').appendChild(anchor)
})();
