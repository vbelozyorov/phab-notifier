#!/bin/sh

PHAB_HOST=$1

build() {
    mkdir -p build
    cp Notifier.user.js Tweaks.user.js build/
    BUILD_TIME=`date +%Y%m%d%H%M`
    echo "Build time is $BUILD_TIME"

    sed -i -- "s/%BUILD_TIME%/$BUILD_TIME/g" build/*.user.js
    sed -i -- "s/%PHAB_HOST%/$PHAB_HOST/g" build/*.user.js

    mv "build/Notifier.user.js" "build/${PHAB_HOST}_notifier.dev.user.js"
    mv "build/Tweaks.user.js" "build/${PHAB_HOST}_tweaks.dev.user.js"

    curl -X POST "https://${BB_AUTH_STRING}@api.bitbucket.org/2.0/repositories/vbelozyorov/phab-notifier/downloads" \
        -F "files=@build/${PHAB_HOST}_notifier.dev.user.js" \
        -F "files=@build/${PHAB_HOST}_tweaks.dev.user.js"
}

echo "Going to dev-build for $PHAB_HOST"
build
