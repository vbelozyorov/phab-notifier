#!/bin/sh

PHAB_HOST=$1

build() {
    mkdir -p build
    cp Notifier.user.js Tweaks.user.js build/
    BUILD_TIME=`date +%Y%m%d%H%M`
    echo "Build time is $BUILD_TIME"

    sed -i -- "s/%BUILD_TIME%/$BUILD_TIME/g" build/*.user.js
    sed -i -- "s/%PHAB_HOST%/$PHAB_HOST/g" build/*.user.js

    local BUILT_COMMIT=`git rev-parse HEAD`
    echo "Built $BUILT_COMMIT"
    echo $BUILT_COMMIT > build/built-commit.txt

    mv "build/built-commit.txt" "build/${PHAB_HOST}_built-commit.txt"
    mv "build/Notifier.user.js" "build/${PHAB_HOST}_notifier.user.js"
    mv "build/Tweaks.user.js" "build/${PHAB_HOST}_tweaks.user.js"

    curl -X POST "https://${BB_AUTH_STRING}@api.bitbucket.org/2.0/repositories/vbelozyorov/phab-notifier/downloads" \
        -F "files=@build/${PHAB_HOST}_built-commit.txt" \
        -F "files=@build/${PHAB_HOST}_notifier.user.js" \
        -F "files=@build/${PHAB_HOST}_tweaks.user.js"
}

get_built_commit() {
   local BUILT_COMMIT=`curl -s -L "https://api.bitbucket.org/2.0/repositories/vbelozyorov/phab-notifier/downloads/${PHAB_HOST}_built-commit.txt" | grep -v " "`
   echo $BUILT_COMMIT
}

BUILT_COMMIT=$(get_built_commit)
echo "Previous build for $PHAB_HOST was from $BUILT_COMMIT"
if [ -z "$BUILT_COMMIT" ]; then
    NEED_BUILD=1
else
    NEED_BUILD=`git diff --name-only $BUILT_COMMIT HEAD | grep -e '\.js$' | wc -l`
fi

if [ $NEED_BUILD -gt 0 ]; then
    echo "Going to build for $PHAB_HOST"
    build
else
    echo "No need to build for $PHAB_HOST again"
fi
